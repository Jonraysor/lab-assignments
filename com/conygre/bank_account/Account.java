package com.conygre.bank_account;

public abstract class Account implements Detailable{

    private double balance;
    private String name;
    private static double interestRate = .01;

    public Account(final String name, final double balance) {
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(final double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public static double getInterestRaste() {
        return interestRate;
    }

    public static void setInterestRaste(final double interestRaste) {
        interestRate = interestRate;
    }

    public abstract void addInterest();

    public boolean withdraw(final double amount) {
        if (balance > amount){
            balance -= amount;
            return true;
        }
        return false;
    }

    public boolean withdraw(){
        if (withdraw(20) == true){
            balance-=20;
            return true;
        }
        return false;
    }

    @Override
    public String getDetails() {
       
        return "Name: " + name + " Balance: " + balance + " Interest Rate:" + interestRate;
       
    }
}