package com.conygre.bank_account;

import java.util.Comparator;
import java.util.TreeSet;

public class CollectionsTest {

    public static void main(String[] args){
        TreeSet<Account> accounts;

        Comparator<Account> compare = (Account acct1, Account acct2) -> {

            if(acct1.getBalance() < acct2.getBalance())
            return -1;
            else if (acct1.getBalance() > acct2.getBalance())
            return 1;
            else return 0;

        };

        accounts = new TreeSet<Account>(compare);

        SavingsAccount acct1 = new SavingsAccount("Jonathan", 800);
        CurrentAccount acct2 = new CurrentAccount("Jane", 700);
        SavingsAccount acct3 = new SavingsAccount("Amier", 750);

        accounts.add(acct1);
        accounts.add(acct2);
        accounts.add(acct3);

            accounts.forEach(account->{ 

                System.out.println("Name: " + account.getName() + " Balance: " + account.getBalance() + "\n");
                account.addInterest();
                System.out.println("Balance after interest payment: " + account.getBalance() + "\n");
            });
        
}
    
}