package com.conygre.bank_account;

public class CurrentAccount extends Account {
    
    public CurrentAccount(String name, double balance){
        super(name, balance);
    }

    public void addInterest(){
        setBalance(getBalance() * 1.1);
    }
}