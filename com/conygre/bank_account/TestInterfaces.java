package com.conygre.bank_account;

public class TestInterfaces {

    public static void main(String[] args){

        Detailable[] details;

        details = new Detailable[3];

        details[0] = new CurrentAccount("Jonathan", 700);
        details[1] = new SavingsAccount("Jane", 600);
        details[2] = new HomeInsurance(700, 300, 1500000);

        for (Detailable d : details){

            System.out.println(d.getDetails() + "\n");
            

        }

    }
    
}