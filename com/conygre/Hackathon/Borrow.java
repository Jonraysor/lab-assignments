package com.conygre.Hackathon;

public interface Borrow {
    
    public abstract boolean takeItem();
    public abstract void returnItem();
}