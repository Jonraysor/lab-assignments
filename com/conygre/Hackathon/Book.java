package com.conygre.Hackathon;

public class Book extends LibraryObject implements Borrow {

    private final int pages;
    private final String author;
    
    Book(String title, int copies, int pages, String author){

        super(title, copies);
        this.pages = pages;
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public boolean takeItem(){

        if (this.getCopies() > 0){
            this.setCopies(this.getCopies()-1);
            return true;
        }
        return false;
    }

    @Override
    public void returnItem(){

        this.setCopies(this.getCopies() + 1);

    }

}