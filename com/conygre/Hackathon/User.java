package com.conygre.Hackathon;


import java.util.HashSet;

public class User{

    private final String USER_ID; 
    private final String NAME;
    private HashSet<LibraryObject> borrowedObjects;

    public User(final String user_id, final String name) {
        
        this.NAME = name;
        this.USER_ID = user_id;
        borrowedObjects = new HashSet<LibraryObject>();

    }

	public String getUSER_ID() {
		return USER_ID;
	}

	public String getNAME() {
		return NAME;
	}

	public HashSet<LibraryObject> getBorrowedObjects() {
		return borrowedObjects;
	}

	public void setBorrowedObjects(HashSet<LibraryObject> borrowedObjects) {
		this.borrowedObjects = borrowedObjects;
	}

    public void borrowObject(LibraryObject object){

        if(borrowedObjects.contains(object) == false)
            borrowedObjects.add(object);

        else
            System.out.println(object.getTitle() + " is already in your possession." + "\n");
    }

    public void returnObject(LibraryObject object){

        if(borrowedObjects.contains(object) == true)
            borrowedObjects.remove(object);

        else
            System.out.println("You do not currently have this item: " + object.getTitle() + " in your possession");

    }

}