package com.conygre.Hackathon;

public class Dvd extends LibraryObject implements Borrow {

    private final String PRODUCER;
    private final int RUN_TIME;

    public Dvd(String title, int copies, String producer, int runTime){

        super(title,copies);

        this.PRODUCER = producer;
        this.RUN_TIME = runTime;

    }

    public String getPRODUCER() {
        return PRODUCER;
    }

    public int getRUN_TIME() {
        return RUN_TIME;
    }

    @Override
    public boolean takeItem(){

        if (this.getCopies() > 0){
            this.setCopies(this.getCopies()-1);
            return true;
        }
        return false;

    }

    @Override
    public void returnItem(){

        this.setCopies(this.getCopies() + 1);

    }
    
}