package com.conygre.Hackathon;

public abstract class LibraryObject{


    private String title;
    private int copies;

    // Super constructor
    LibraryObject(final String title, final int copies) {

        this.title = title;
        this.copies = copies;
        
    }

    // Methods for all library objects

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(final int copies) {
        this.copies = copies;
    }

    public boolean copyAvailable(LibraryObject obj){
        if (obj.getCopies() > 1)
            return true;
        return false;
    }

}