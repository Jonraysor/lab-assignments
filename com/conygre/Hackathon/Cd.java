package com.conygre.Hackathon;

public class Cd extends LibraryObject implements Borrow{

    private final String ARTIST;
    private final int TRACKS;

    public Cd(String title, int copies, String artist, int tracks){

        super(title, copies);
        
        this.ARTIST = artist;
        this.TRACKS = tracks;

    }

    public String getARTIST() {
        return ARTIST;
    }

    public int getTRACKS() {
        return TRACKS;
    }

    @Override
    public boolean takeItem(){

        if (this.getCopies() > 0){
            this.setCopies(this.getCopies()-1);
            return true;
        }
        return false;

    }

    @Override
    public void returnItem(){

        this.setCopies(this.getCopies() + 1);

    }
    
}