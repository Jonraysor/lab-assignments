package com.conygre.Hackathon;

public class Periodicals extends LibraryObject{

    private final String PUBLISHER;
    private final int ISSUE;

    public Periodicals(String title, int copies, String publisher, int issue) {
        super(title, copies);
        this.ISSUE = issue;
        this.PUBLISHER = publisher;
    }

    public String getPUBLISHER() {
        return PUBLISHER;
    }

    public int getISSUE() {
        return ISSUE;
    }
    


    
}