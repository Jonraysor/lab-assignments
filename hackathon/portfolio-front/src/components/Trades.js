import React from 'react'

const Trades = ({ trades }) => {
  return (
    <div>
      <center><h1>Portfolio</h1></center>
      {trades.map((trade) => (
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Ticker: {trade.ticker}</h5>
            <h6 class="card-subtitle mb-2 text-muted"> Price: {trade.price}</h6>
            <h6 class="card-subtitle mb-2 text-muted"> Status: {trade.state}</h6>
            <h6 class="card-subtitle mb-2 text-muted"> Value: {trade.value}</h6>
            <p class="card-text">Quantity: {trade.quantity}</p>
          </div>
        </div>
      ))}
    </div>
  )
};

export default Trades