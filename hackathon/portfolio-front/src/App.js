import React, {Component} from 'react';
import Trades from './components/Trades';
class App extends Component {

  state = {

    trades: []

  }

  componentDidMount() {
    fetch('http://localhost:8080/trades/portfolio')
    .then(res => res.json())
    .then((data) => {

      //console.log(data);
      this.setState({ trades : data})
    })
    .catch(console.log)
  }
  
  
  render () {
    return (
      <Trades trades = {this.state.trades}/>
    );
  }
}


export default App;
