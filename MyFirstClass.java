public class MyFirstClass
{

    public static void main (String[] args){

        String make = "Subaru";
        String model = "WRX";
        double engineSize = 2.0;
        byte gear = 3;

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        short speed;
        speed  = (short)(gear * 20);

        System.out.println("The current speed is: " + speed);



    }
}